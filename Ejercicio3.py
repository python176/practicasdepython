#Pedir que el usuario ingrese una cadena de caracteres, que el programa cuente cuantos caracteres tiene 
# y cuantas veces aparece la letra A(mayuscula y minuscula)

cadena = input("Ingrese una cadena de caracteres: ")

cuenta = 0
cuenta2 = 0
cuenta3 = 0
for carac in cadena:
    if carac == 'a':
        cuenta += 1
    elif carac == "A":
        cuenta2 += 1
    cuenta3 += 1

   
print(f"Existen {cuenta} caracteres con la letra a en {cadena}, y {cuenta2} caracteres con la letra A en mayuscula.")
print(f"El total de caracteres en {cadena} es de {cuenta3}.")

