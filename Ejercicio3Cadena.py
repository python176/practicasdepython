# Hay dos formas, la que describo en el codigo y la otra usando es len y count (opcionalmente, tambien lower)
palabra=input('Tecla la cadena: ')
cont_p=0
cont_a=0
for p in palabra:
    cont_p+=1
    if p in ['a', 'A']:
        cont_a +=1
print(f'En la cadena "palabra", hay {cont_p} caracteres y {cont_a} "a" y/o "A"')
