## Calcular el área de un circulo, con el RADIO ingresado por el usuario
radio = float(input("Ingresa el radio del circulo: "))
areaCirculo = float((radio*radio)* 3.1416)
print("para el radio {0:0.4f} el área es: {1:0.4f}" .format(radio, areaCirculo))
